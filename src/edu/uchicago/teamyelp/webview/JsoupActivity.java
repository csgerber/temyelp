package edu.uchicago.teamyelp.webview;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import edu.uchicago.teamyelp.R;
import edu.uchicago.teamyelp.StartSearchActivity;

public class JsoupActivity extends Activity {
    private TextView mRestaurantNameTextView, mRestaurantAddressTextView, mRestaurantPhoneTextView;
    private Button mbackButton;
//    private WebView mWebView;

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jsoup_result);
        mRestaurantNameTextView = (TextView) findViewById(R.id.resName);
        mRestaurantAddressTextView = (TextView) findViewById(R.id.resAddress);
        mRestaurantPhoneTextView = (TextView) findViewById(R.id.resPhone);
        mbackButton = (Button) findViewById(R.id.backButton);

        //        http://www.google.com/search?hl=en&btnI=1&q=medici+chicago+site:yelp.com
        String URL = (
                ("http://www.google.com/search?hl=en&btnI=1&q=")+
                ((String) getIntent().getExtras().get("NAME")).replaceAll(" ", "+")+"+"+
                ((String)getIntent().getExtras().get("LOCATION")).replaceAll(" ", "+")+
                ("+site:yelp.com"));

        new JsoupSearchTask().execute(URL);


        mbackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                back();
            }
        });

//        mWebView = (WebView) findViewById(R.id.webView);
//        mWebView.getSettings().setJavaScriptEnabled(true);

//        mWebView.setWebViewClient(new WebViewClient()){
//            public boolean shouldOverrideUrlLoading(WebView view, String url){
//            return false;
//        }
//    };
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

    }

    private class JsoupSearchTask extends AsyncTask<String, Void, ArrayList<String>> {
        @Override
        protected ArrayList<String> doInBackground(String... strings) {
            String URL = strings[0];
            ArrayList<String> result = new ArrayList<String>();
            try {
                Document doc = Jsoup.connect(URL).userAgent("Mozilla").get();
                Elements resName = doc.select("h1[itemprop=name]");
                Elements resAddress = doc.select("address[itemprop=address]");
                Elements resPhone = doc.select("span[itemprop=telephone]");
                result.add(resName.text());
                result.add(resAddress.text());
                result.add(resPhone.text());
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(ArrayList<String> s) {
            mRestaurantNameTextView.setText(s.get(0));
            mRestaurantAddressTextView.setText(s.get(1));
            mRestaurantPhoneTextView.setText(s.get(2));
        }
    }

    private void back(){
        Intent intent = new Intent(this,StartSearchActivity.class );
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

}
